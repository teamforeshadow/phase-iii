/*
____ ____ ____ ____ ____ _  _ ____ ___  ____ _ _ _
|___ |  | |__/ |___ [__  |__| |__| |  \ |  | | | |
|    |__| |  \ |___ ___] |  | |  | |__/ |__| |_|_|

*/

/*
Foreshadow Middleware API
Created by: Team Foreshadow - IBM's Unchain the Mainframe 2017
Contributors:
  David Kennedy,
  Michael Machnik,
  Andrea Saxman,
  Joseph Parisi,
  Daryl Stanton

Purpose: The functions below act as RESTful controls.
  1. Controls inbound data injestion and placement in MongoDB for temporary storage.
  2. Controls invoking adding new records to the Foreshadow Blockchain.
  3. Controls Foreshadow UI SMF Filter requests.

*/

'use strict';

var mongoose = require('mongoose'),
  Task = mongoose.model('Tasks');

var hfc = require('../hyperledger/node_modules/fabric-client');
var path = require('../hyperledger/node_modules/path');
var util = require('../hyperledger/node_modules/util');
var _ = require('../hyperledger/node_modules/lodash');
var request = require("../hyperledger/node_modules/request");
var moment = require("../hyperledger/node_modules/moment");
var DiscoveryV1 = require('../../node_modules/watson-developer-cloud/discovery/v1');

exports.list_all_tasks = function(req, res) {
  Task.find({}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.create_a_task = function(req, res) {
  var new_task = new Task(req.body);
  new_task.save(function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.read_a_task = function(req, res) {
  Task.findById(req.params.taskId, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.update_a_task = function(req, res) {
  Task.findOneAndUpdate({
    _id: req.params.taskId
  }, req.body, {
    new: true
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.delete_a_task = function(req, res) {

  Task.remove({
    _id: req.params.taskId
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({
      message: 'Record successfully deleted'
    });
  });
};

exports.read_hyperledger = function(req, res) {

  //use for testing request parameters to hyperledger
  console.log(req.params.query);

  var query = req.params.query;

  var options = {
    wallet_path: "../hyperledger/fabric-samples/fabcar/creds",
    user_id: 'PeerAdmin',
    channel_id: 'mychannel',
    chaincode_id: 'fabcar',
    network_url: 'grpc://localhost:7051',
  };

  var channel = {};
  var client = null;
  var queryType = null;
  var filter = null;
  var filterKey = null;
  var filterValue = null;

  console.log("query:" + query);
  console.log(process.argv);

  if (query == "all") {

    queryType = 'queryAllSMF';
    query = '';

  } else {

    queryType = 'querySMF';
    query = query;

  }

  if (process.argv[4] == "filter") {

    var filterKey = process.argv[5];
    var filterValue = process.argv[6];

    console.log("filterKey:" + filterKey);
    console.log("filterValue:" + filterValue);

  }

  Promise.resolve().then(() => {
    console.log("Create a client and set the wallet location");
    client = new hfc();
    return hfc.newDefaultKeyValueStore({
      path: options.wallet_path
    });
  }).then((wallet) => {
    console.log("Set wallet path, and associate user ", options.user_id, " with application");
    client.setStateStore(wallet);
    return client.getUserContext(options.user_id, true);
  }).then((user) => {
    console.log("Check user is enrolled, and set a query URL in the network");
    if (user === undefined || user.isEnrolled() === false) {
      console.error("User not defined, or not enrolled - error");
    }
    channel = client.newChannel(options.channel_id);
    channel.addPeer(client.newPeer(options.network_url));
    return;
  }).then(() => {
    console.log("Make query");
    var transaction_id = client.newTransactionID();
    console.log("Assigning transaction_id: ", transaction_id._transaction_id);

    //Foreshadow query all SMF Records

    /*
        const request = {
            chaincodeId: options.chaincode_id,
            txId: transaction_id,
            fcn: 'queryAllCars',
            args: ['']
        };

    */

    //Foreshadow query SMF Records Dynamically

    const request = {
      chaincodeId: options.chaincode_id,
      txId: transaction_id,
      fcn: queryType,
      args: [query]
    };

    return channel.queryByChaincode(request);
  }).then((query_responses) => {
    console.log("returned from query");
    if (!query_responses.length) {
      console.log("No payloads were returned from query");
    } else {
      console.log("Query result count = ", query_responses.length)
    }
    if (query_responses[0] instanceof Error) {
      console.error("error from query = ", query_responses[0]);
    }

    //helper to validate Hyperledger items
    //console.log(query_responses[0].toString());

    //return Hyperledger item to Foreshadow UX
    res.json(JSON.parse(query_responses[0]));

  }).catch((err) => {
    console.error("Caught Error", err);
  });

};

exports.write_hyperledger = function(req, res) {

  /* Mock SMF Mainframe Record

  VALID JSON (RFC 4627)
  Formatted JSON Data
  {
     "_id":"59d42f823c1df806035e5e09",
     "RECORD":{
        "SYSTEM":"CW09",
        "REGION":"PMIMAMFD",
        "ZOSSERVERID":"FZID",
        "ID":"0001",
        "STARTTIME":"09.53.06.1236",
        "ENDTIME":"09:54:25.1534",
        "TYPE":"WITHDRAWL",
        "JOBNAME":"ZZZZZZZZ",
        "UID":"PMIMAM0"
     },
     "__v":0,
     "status":[
        "pending"
     ],
     "Created_date":"2017-10-04T00:46:58.358Z"
  }

  //Foreshadow Mock request
  //http://10.101.16.185:3000/hyperledger/add/59d42f823c1df806035e5e09/cw09/PMIMAMFD/FZIS/10042017/10042017/WITHDRAW/ZZZZZZZZ/SMIDXK0

  //Debug inbound POST method
  //Log via SumoLogic all network requests to blockchain
  //console.log(req.params.add);

  //Foreshadow router path reference
  //hyperledger/add/:id/:system/:region/:zosserverid/:startime/:endtime/:type/:jobname/:uid')

*/

  var smf = {

    "SRVR_JOBNAME": req.params.SRVR_JOBNAME,
    "SRVR_ID": req.params.SRVR_ID,
    "SRVR_SYSTEM": req.params.SRVR_SYSTEM,
    "PRODNAME": req.params.PRODNAME,
    "APPLID": req.params.APPLID,
    "JOBNAME": req.params.JOBNAME,
    "TRAN": req.params.TRAN,
    "PROGRAMNAME": req.params.PROGRAMNAME,
    "ORIGINATINGUSER": req.params.ORIGINATINGUSER,
    "USERID": req.params.USERID,
    "STARTTIME": req.params.STARTTIME,
    "ELAPSEDTIME": req.params.ELAPSEDTIME,

  }

  //Debug inbound SMF record
  console.log(smf);

  var options = {
    wallet_path: "../hyperledger/fabric-samples/fabcar/creds",
    user_id: 'PeerAdmin',
    channel_id: 'mychannel',
    chaincode_id: 'fabcar',
    peer_url: 'grpc://localhost:7051',
    event_url: 'grpc://localhost:7053',
    orderer_url: 'grpc://localhost:7050'
  };

  var channel = {};
  var client = null;
  var targets = [];
  var tx_id = null;
  Promise.resolve().then(() => {
    console.log("Create a client and set the wallet location");
    client = new hfc();
    return hfc.newDefaultKeyValueStore({
      path: options.wallet_path
    });
  }).then((wallet) => {
    console.log("Set wallet path, and associate user ", options.user_id, " with application");
    client.setStateStore(wallet);
    return client.getUserContext(options.user_id, true);
  }).then((user) => {
    console.log("Check user is enrolled, and set a query URL in the network");
    if (user === undefined || user.isEnrolled() === false) {
      console.error("User not defined, or not enrolled - error");
    }
    channel = client.newChannel(options.channel_id);
    var peerObj = client.newPeer(options.peer_url);
    channel.addPeer(peerObj);
    channel.addOrderer(client.newOrderer(options.orderer_url));
    targets.push(peerObj);
    return;
  }).then(() => {
    tx_id = client.newTransactionID();
    console.log("Assigning transaction_id: ", tx_id._transaction_id);

    //Proof of Concept - Progressivly keep 1000 SMF records on Hyperledger

    var rNum = Math.floor(Math.random() * 10000);
    var smfNum = "SMF" + rNum.toString();
    var smfID = smfNum;

    // var request = {
    //   targets: targets,
    //   chaincodeId: options.chaincode_id,
    //   fcn: 'createSMF',
    //   args: [
    //     smfID,
    //     smf.system.toUpperCase(),
    //     smf.region,
    //     smf.zosserverid,
    //     smf.startime,
    //     smf.endtime,
    //     smf.type,
    //     smf.jobname,
    //     smf.uid
    //   ],
    //   chainId: options.channel_id,
    //   txId: tx_id
    // };

    var request = {
      targets: targets,
      chaincodeId: options.chaincode_id,
      fcn: 'createSMF',
      args: [
        smfID,
        smf.SRVR_JOBNAME,
        smf.SRVR_ID,
        smf.SRVR_SYSTEM,
        smf.PRODNAME,
        smf.APPLID,
        smf.JOBNAME,
        smf.TRAN,
        smf.PROGRAMNAME,
        smf.ORIGINATINGUSER,
        smf.USERID,
        smf.STARTTIME,
        smf.ELAPSEDTIME
      ],
      chainId: options.channel_id,
      txId: tx_id
    };

    return channel.sendTransactionProposal(request);
  }).then((results) => {
    var proposalResponses = results[0];
    var proposal = results[1];
    var header = results[2];
    let isProposalGood = false;
    if (proposalResponses && proposalResponses[0].response &&
      proposalResponses[0].response.status === 200) {
      isProposalGood = true;
      console.log('transaction proposal was good');
    } else {
      console.error('transaction proposal was bad');
    }
    if (isProposalGood) {
      console.log(util.format(
        'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s", metadata - "%s", endorsement signature: %s',
        proposalResponses[0].response.status, proposalResponses[0].response.message,
        proposalResponses[0].response.payload, proposalResponses[0].endorsement.signature));
      var request = {
        proposalResponses: proposalResponses,
        proposal: proposal,
        header: header
      };
      // set the transaction listener and set a timeout of 30sec
      // if the transaction did not get committed within the timeout period,
      // fail the test
      var transactionID = tx_id.getTransactionID();
      var eventPromises = [];
      let eh = client.newEventHub();
      eh.setPeerAddr(options.event_url);
      eh.connect();

      let txPromise = new Promise((resolve, reject) => {
        let handle = setTimeout(() => {
          eh.disconnect();
          reject();
        }, 30000);

        eh.registerTxEvent(transactionID, (tx, code) => {
          clearTimeout(handle);
          eh.unregisterTxEvent(transactionID);
          eh.disconnect();

          if (code !== 'VALID') {
            console.error(
              'The transaction was invalid, code = ' + code);
            reject();
          } else {
            console.log(
              'The transaction has been committed on peer ' +
              eh._ep._endpoint.addr);
            resolve();
          }
        });
      });
      eventPromises.push(txPromise);
      var sendPromise = channel.sendTransaction(request);
      return Promise.all([sendPromise].concat(eventPromises)).then((results) => {
        console.log(' event promise all complete and testing complete');
        return results[0]; // the first returned value is from the 'sendPromise' which is from the 'sendTransaction()' call
      }).catch((err) => {
        console.error(
          'Failed to send transaction and get notifications within the timeout period.'
        );
        return 'Failed to send transaction and get notifications within the timeout period.';
      });
    } else {
      console.error(
        'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...'
      );
      return 'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...';
    }
  }, (err) => {
    console.error('Failed to send proposal due to error: ' + err.stack ? err.stack :
      err);
    return 'Failed to send proposal due to error: ' + err.stack ? err.stack :
      err;
  }).then((response) => {
    if (response.status === 'SUCCESS') {
      console.log('Successfully sent transaction to Foreshadow Hyperledger.');
      //res.json('Successfully sent transaction to Foreshadow.');
      return tx_id.getTransactionID();
    } else {
      console.error('Failed to order the transaction. Error code: ' + response.status);
      return 'Failed to order the transaction. Error code: ' + response.status;
    }
  }, (err) => {
    console.error('Failed to send transaction due to error: ' + err.stack ? err
      .stack : err);
    return 'Failed to send transaction due to error: ' + err.stack ? err.stack :
      err;

  });

}

exports.sync_hyperledger = function(req, res) {

  var getSMF = function() {

    return new Promise(function(resolve, reject) {

      var options = {
        method: 'GET',
        url: 'http://smf.foreshadow-unchain.com:443/tasks'
      };

      request(options, function(error, response, body) {
        //if (error) throw new Error(error);
        if (error) reject(error);

        var blockData = JSON.parse(body).slice(11, 1100);

        console.log(blockData);

        resolve(blockData);

      });
    })
  }

  var hyperBlock = function(result) {

    var promises = [];
    for (var i = 0; i < result.length; i++) {

      //Convert SMF records to acceptable case sensitive format

      console.log(result[0].RECORDS[0].SRVR_JOBNAME);

      //console.log(result[i].RECORDS.SRVR_JOBNAME)

      var req = {
        params: {

          "SRVR_JOBNAME": result[i].RECORDS[0].SRVR_JOBNAME,
          "SRVR_ID": result[i].RECORDS[0].SRVR_ID,
          "SRVR_SYSTEM": result[i].RECORDS[0].SRVR_ID,
          "PRODNAME": result[i].RECORDS[0].PRODNAME,
          "APPLID": result[i].RECORDS[0].APPLID,
          "JOBNAME": result[i].RECORDS[0].JOBNAME,
          "TRAN": result[i].RECORDS[0].TRAN,
          "PROGRAMNAME": result[i].RECORDS[0].PROGRAMNAME,
          "ORIGINATINGUSER": result[i].RECORDS[0].ORIGINATINGUSER,
          "USERID": result[i].RECORDS[0].USERID,
          "STARTTIME": result[i].RECORDS[0].STARTTIME,
          "ELAPSEDTIME": result[i].RECORDS[0].ELAPSEDTIME

        }
      };

      var promise = exports.write_hyperledger(req);
      promises.push(promise);
    }

    Promise.all(promises).then(res.json("MongoDB SMF records stash has been synced to Hyperledger, and all records have been accepted."));

  }

  getSMF()
    .then(function(result) {
      return hyperBlock(result)
    })

}

exports.hyperledger_analytics = function(req, res) {

  var A = function() {

    return new Promise(function(resolve, reject) {

      var options = {
        method: 'GET',
        url: 'http://localhost:3000/hyperledger/all'
      };

      request(options, function(error, response, body) {
        //if (error) throw new Error(error);
        if (error) reject(error);

        console.log(body);

        var hyperblock = JSON.parse(body).slice(1, 100);

        console.log(hyperblock);

        resolve(hyperblock);

      });

    })
  }

  var B = function(hyperblock) {

    return new Promise(function(resolve, reject) {

      //console.log(hyperblock);
      //console.log(hyperblock.length);

      var records = _.flatMap(hyperblock);

      var smfObject = null;
      var smfArray = [];

      _.forEach(records, function(record) {

        //Determine SMF Tx Response Time

        //Format SMF Mainframe timestamp
        //var start = "09.54.06.1236"
        //var end = "09.54.30.1236"

         //var start = record.Record.STARTTIME;
        //var end = record.Record.ENDTIME;
        var elapsed = record.Record.ELAPSEDTIME;

        // var startStub = start.split('.').join(':');
        // var endStub = end.split('.').join(':');
        var elapsedStub = elapsed.split('.').join(':');

        smfObject = {

          "key": record.Key,
          "SRVR_JOBNAME": record.Record.SRVR_JOBNAME,
          "SRVR_ID": record.Record.SRVR_ID,
          "SRVR_SYSTEM": record.Record.SRVR_SYSTEM,
          "PRODNAME": record.Record.PRODNAME,
          "APPID": record.Record.APPID,
          "JOBNAME": record.Record.JOBNAME,
          "TRAN": record.Record.TRAN,
          "PROGRAMNAME": record.Record.PROGRAMNAME,
          "performance": elapsedStub,
          "ORIGINATINGUSER": record.Record.ORIGINATINGUSER,
          "USERID": record.Record.USERID,
          "STARTTIME": record.Record.STARTTIME,
          "ELAPSEDTIME": record.Record.ELAPSEDTIME
          //"pRaw": pNumber,
          //"d": d
        }

        smfArray.push(smfObject);

      });

      //Calculate SMF Analytics for Foreshadow UX
      //Preparing data to be shipped to Watson AI for Machine Learning for further insights.
      //Phase III awaits!

      //Define CICS Regions with Foreshadow enabled.
      //CICS Transaction volume tracking
      var cw09 = _.filter(smfArray, ['SRVR_SYSTEM', "CW06"]);
      var cw11 = _.filter(smfArray, ['SRVR_SYSTEM', "FZMM"]);

      //zOS Server Transaction volume tracking
      var fzid = _.filter(smfArray, ['SRVR_ID', "FZID"]);
      var fzmm = _.filter(smfArray, ['SRVR_ID', "FZMM"]);

      //Hyperledger transaction type analytics
      var withdraw = _.filter(smfArray, ['PROGRAMNAME', "ESDASM50"]);
      var deposit = _.filter(smfArray, ['JOBNAME', "UCICS69M"]);
      var check = _.filter(smfArray, ['PRODNAME', "H06AC603"]);

      //Determine Today's Average Tx Response Time

      //var txToday = _.mean(_.map(smfArray, 'pRaw'));
      //var txToday = _.mean(_.map(smfArray, 'ELAPSEDTIME'));
      //var txToday = _.mean(["00:08:03:3764","00:08:03:3764"]);
      //var txTest = _.map(smfArray, 'ELAPSEDTIME');
      //console.log(txTest);
      //console.log("txtoday: " + txToday.toString());
      //var average = _.mean(numbers);
      //var average2 = _.mean(txToday);

      //Return Foreshadow analytics to UX
      var analytics = {

        "smfCount": smfArray.length,
        "cw09Volume": cw09.length,
        "cw11Volume": cw11.length,
        "fzid": fzid.length,
        "fzmm": fzmm.length,
        "withdraw": withdraw.length,
        "deposit": deposit.length,
        "check": check.length,
        "smf": smfArray,
        "txToday": txToday.toFixed(0)
      };

      resolve(analytics);

    })
  }

  var C = function(result) {

    return new Promise(function(resolve, reject) {

      resolve(result);

    })
  }

  A()
    .then(function(result) {
      return B(result)
    }).then(function(result) {
      res.json(result)
    });

}

exports.watson = function(req, res) {

  //Slack Notification for Foreshadow Watson based Machine Learning and Analytics

  //Webhook URL
    //https://hooks.slack.com/services/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

  //channel
      //mainframe-devops

  var positive = new Promise((resolve, reject) => {

    var options = { method: 'GET',
  url: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/f17611a6-ff04-464d-9792-73846a1a0d34/collections/6a7ff7af-2122-400e-a0c3-427392db9e73/query',
  qs:
   { version: '2017-10-16',
     count: '100',
     offset: '',
     aggregation: '',
     filter: 'Record.SRVR_ID:"FZMM"',
     passages: 'true',
     deduplicate: 'false',
     highlight: 'true',
     return: '',
     query: 'enriched_Record_sentiment.sentiment.document.label:"positive"' },
  headers:
   { 'postman-token': 'b859aff6-5d11-e61a-0a59-e36d616c468e',
     'cache-control': 'no-cache',
     authorization: 'Basic MTMxOTVkZjYtODEwYy00NmU2LTk0YTYtZmNkYzFjNmU3ZDU0Om84U1A1cnFXOGtKbw==' } };

        request(options, function (error, response, body) {
        if (error) throw new Error(error);

        var json = JSON.parse(body);


            resolve(json.matching_results)

  });


  });

    var neutral = new Promise((resolve, reject) => {

          var options = { method: 'GET',
  url: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/f17611a6-ff04-464d-9792-73846a1a0d34/collections/6a7ff7af-2122-400e-a0c3-427392db9e73/query',
  qs:
   { version: '2017-10-16',
     count: '100',
     offset: '',
     aggregation: '',
     filter: 'Record.SRVR_ID:"FZMM"',
     passages: 'true',
     deduplicate: 'false',
     highlight: 'true',
     return: '',
     query: 'enriched_Record_sentiment.sentiment.document.label:"neutral"' },
  headers:
   { 'postman-token': 'b859aff6-5d11-e61a-0a59-e36d616c468e',
     'cache-control': 'no-cache',
     authorization: 'Basic MTMxOTVkZjYtODEwYy00NmU2LTk0YTYtZmNkYzFjNmU3ZDU0Om84U1A1cnFXOGtKbw==' } };

          request(options, function (error, response, body) {
          if (error) throw new Error(error);

          var json = JSON.parse(body);


              resolve(json.matching_results)

  });



  });

    var negative = new Promise((resolve, reject) => {

          var options = { method: 'GET',
  url: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/f17611a6-ff04-464d-9792-73846a1a0d34/collections/6a7ff7af-2122-400e-a0c3-427392db9e73/query',
  qs:
   { version: '2017-10-16',
     count: '100',
     offset: '',
     aggregation: '',
     filter: 'Record.SRVR_ID:"FZMM"',
     passages: 'true',
     deduplicate: 'false',
     highlight: 'true',
     return: '',
     query: 'enriched_Record_sentiment.sentiment.document.label:"negative"' },
  headers:
   { 'postman-token': 'b859aff6-5d11-e61a-0a59-e36d616c468e',
     'cache-control': 'no-cache',
     authorization: 'Basic MTMxOTVkZjYtODEwYy00NmU2LTk0YTYtZmNkYzFjNmU3ZDU0Om84U1A1cnFXOGtKbw==' } };

          request(options, function (error, response, body) {
          if (error) throw new Error(error);

          var json = JSON.parse(body);

              resolve(json.matching_results)

  });


  });


  var A = function () {

      return new Promise(function (resolve, reject) {


        Promise.all([positive, neutral, negative]).then(values => {

          function add(a, b) {
              return a + b;
          }

          var positive = values[0]
          var neutral = values[1]
          var negative = values[2]

          console.log(values);

      //Total number of records with sentiment
          var sum = [positive, neutral, negative].reduce(add, 0);
            console.log(sum);

      // //Positive Percentage

           var pShare = (positive/sum);
           console.log(parseInt(pShare*100));

      // //Neutral Percentage

          var neuShare = (neutral/sum);
          console.log(parseInt(neuShare*100));
      // //Negative Percentage

           var negShare = (negative/sum);
           console.log(parseInt(negShare*100));

           var shares = {

              pos: parseInt(pShare*100),
              neg: parseInt(negShare*100),
              neu: parseInt(neuShare*100)
           }

        //resolve(parseInt(negShare*100));

        resolve(shares)

      });

      })
    }

   var B = function (result) {

      return new Promise(function (resolve, reject) {

        console.log(result);

      if(result.neg >= 10){

        var diff = result.neg - 10;

        var Slack = require('../../node_modules/slack-node');

        var webhookUri = "https://hooks.slack.com/services/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";

        var slack = new Slack();

        slack.setWebhook(webhookUri);

        slack.webhook({
          channel: "#mainframe-devops",
          username: "Watson",
          //text: "Hello! Attention! There seems to be an issue with CW01, and the CWXTCOB job. For the last 5 minutes, 100 users have been affected and are disatisfied with their experience. Please use the following URL understand more. Hopefully, this information doesn't ruin your day."
          text: "Attention! Negative sentiment is " + diff + " greater than the norm. Please use Foreshadow or Watson to determing the root cause."

        }, function(err, response) {
          console.log(response);
        });

          resolve("Sent message to Slack");
          res.json("Foreshadow and Watson are now processing your request.");

        } else {

          var Slack = require('../../node_modules/slack-node');

          var webhookUri = "https://hooks.slack.com/services/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";

          var slack = new Slack();

          slack.setWebhook(webhookUri);

          slack.webhook({
            channel: "#mainframe-devops",
            username: "Watson",
            //text: "Hello! Attention! There seems to be an issue with CW01, and the CWXTCOB job. For the last 5 minutes, 100 users have been affected and are disatisfied with their experience. Please use the following URL understand more. Hopefully, this information doesn't ruin your day."
            text: "All systems are performing well. Positive sentiment is at " + result.pos + " percent and negative sentiment is at " + result.neg + " percent. Have a great day!"

          }, function(err, response) {
            console.log(response);
          });

          console.log("Sentiment within tolerance. No messages sent.")
          res.json("Data within tolerance");

        }

      })
    }

    A()
     .then(function (result) {
       return B(result);
     });

}

exports.watson_relative = function(req, res) {

//Standalone Dynamic Query Endpoint to gather insights from any timeframe.

var interval = req.params.interval

  //Slack Notification for Foreshadow Watson based Machine Learning and Analytics

  //Webhook URL
    //https://hooks.slack.com/services/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

  //channel
      //mainframe-devops

  //get date hour - 1

  var n = moment().utc().format("YYYY-MM-DDThh")
  var n = moment(n).subtract(interval, 'hours').format("YYYY-MM-DDThh");


  // var pRelative = [];
  // var neuRelative = [];
  // var negRelative = [];

  //'opSystem::"PROD", jobDate:"2017-11-07T13"'

  //var filter = `opSystem::"PROD", jobDate:"${n}"`
    var filter = `opSystem::"PROD"`
  console.log(filter);

  var positive = new Promise((resolve, reject) => {

    var options = { method: 'GET',
  url: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/f17611a6-ff04-464d-9792-73846a1a0d34/collections/636ffdcc-94f8-4752-b10c-8c143df671a0/query',
  qs:
  { version: '2017-10-16',
   count: '',
   offset: '',
   aggregation: '',
   filter: filter,
   passages: 'true',
   deduplicate: 'false',
   highlight: 'true',
   return: '',
   query: 'enriched_sentiment.sentiment.document.label::"positive"' },
  headers:
  { 'postman-token': 'bb097f8a-9529-65c6-446b-93767f738b92',
   'cache-control': 'no-cache',
   authorization: 'Basic MTMxOTVkZjYtODEwYy00NmU2LTk0YTYtZmNkYzFjNmU3ZDU0Om84U1A1cnFXOGtKbw==' } };

        request(options, function (error, response, body) {
        if (error) throw new Error(error);

        var json = JSON.parse(body);

       //console.log(json.results)

      //var prelative = [];

      var cw01 = _.filter(json.results, ['systemID', 'cw01']).length;
      var cw09 = _.filter(json.results, ['systemID', 'cw09']).length;
      var cw11 = _.filter(json.results, ['systemID', 'cw11']).length;
      var cw70 = _.filter(json.results, ['systemID', 'cw70']).length;


      var positive = {

        cw01: {positive : cw01},
        cw09: {positive : cw09},
        cw11: {positive : cw11},
        cw70: {positive : cw70},

      };

            resolve(positive)

  });


  });

    var neutral = new Promise((resolve, reject) => {

          var options = { method: 'GET',
          url: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/f17611a6-ff04-464d-9792-73846a1a0d34/collections/636ffdcc-94f8-4752-b10c-8c143df671a0/query',
          qs:
          { version: '2017-10-16',
          count: '',
          offset: '',
          aggregation: '',
          //filter: 'opSystem::"PROD", jobDate:"2017-11-07T13"',
          filter: filter,
          passages: 'true',
          deduplicate: 'false',
          highlight: 'true',
          return: '',
          query: 'enriched_sentiment.sentiment.document.label::"neutral"' },
          headers:
          { 'postman-token': 'f2d29730-9c6b-8b19-9119-4adc134df1d8',
          'cache-control': 'no-cache',
          authorization: 'Basic MTMxOTVkZjYtODEwYy00NmU2LTk0YTYtZmNkYzFjNmU3ZDU0Om84U1A1cnFXOGtKbw==' } };

          request(options, function (error, response, body) {
          if (error) throw new Error(error);

          var json = JSON.parse(body);

          var relative = [];

          var cw01 = _.filter(json.results, ['systemID', 'cw01']).length;
          var cw09 = _.filter(json.results, ['systemID', 'cw09']).length;
          var cw11 = _.filter(json.results, ['systemID', 'cw11']).length;
          var cw70 = _.filter(json.results, ['systemID', 'cw70']).length;

          var neutral = {

            cw01: {neutral : cw01},
            cw09: {neutral : cw09},
            cw11: {neutral : cw11},
            cw70: {neutral : cw70},

          };

              resolve(neutral)

  });



  });

    var negative = new Promise((resolve, reject) => {

          var options = { method: 'GET',
          url: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/f17611a6-ff04-464d-9792-73846a1a0d34/collections/636ffdcc-94f8-4752-b10c-8c143df671a0/query',
          qs:
          { version: '2017-10-16',
          count: '',
          offset: '',
          aggregation: '',
          //filter: 'opSystem::"PROD", jobDate:"2017-11-07T13"',
          filter: filter,
          passages: 'true',
          deduplicate: 'false',
          highlight: 'true',
          return: '',
          query: 'enriched_sentiment.sentiment.document.label::"negative"' },
          headers:
          { 'postman-token': 'f2d29730-9c6b-8b19-9119-4adc134df1d8',
          'cache-control': 'no-cache',
          authorization: 'Basic MTMxOTVkZjYtODEwYy00NmU2LTk0YTYtZmNkYzFjNmU3ZDU0Om84U1A1cnFXOGtKbw==' } };

          request(options, function (error, response, body) {
          if (error) throw new Error(error);

          var json = JSON.parse(body);

          //console.log(json);

          //var cw01 = _.filter(json.results, ['systemID', 'cw01']);

          //console.log(cw01);

          var cw01 = _.filter(json.results, ['systemID', 'cw01']).length;
          var cw09 = _.filter(json.results, ['systemID', 'cw09']).length;
          var cw11 = _.filter(json.results, ['systemID', 'cw11']).length;
          var cw70 = _.filter(json.results, ['systemID', 'cw70']).length;

          //relative["cw01"] = {negative: cw01};
          //relative["cw09"] = {negative: cw09};
          //relative["cw11"] = {negative: cw11};
          //relative["cw70"] = {negative: cw70};

          var negative = {

            cw01: {negative : cw01},
            cw09: {negative : cw09},
            cw11: {negative : cw11},
            cw70: {negative : cw70},

          };

              resolve(negative)

  });


  });


  var A = function () {

      return new Promise(function (resolve, reject) {


        Promise.all([positive, neutral, negative]).then(values => {

          //function add(a, b) {
          //    return a + b;
          //}

          //var positive = values[0]
          //var neutral = values[1]
          //var negative = values[2]

          //var flatR = _.flatten(values);

          //console.log(flatR);

          //console.log(values);

      //Total number of records with sentiment
          //var sum = [positive, neutral, negative].reduce(add, 0);
            //console.log(sum);

      // //Positive Percentage

           //var pShare = (positive/sum);
           //console.log(parseInt(pShare*100));

      // //Neutral Percentage

          //var neuShare = (neutral/sum);
          //console.log(parseInt(neuShare*100));
      // //Negative Percentage

           //var negShare = (negative/sum);
           //console.log(parseInt(negShare*100));

        //resolve(parseInt(negShare*100));

        // var sentimentCount = {
        //
        //   p: parseInt(pShare*100),
        //   neu: parseInt(neuShare*100),
        //   neg: parseInt(negShare*100)
        //
        // }

        resolve(values)

      });

      })
    }

   var B = function (result) {

      return new Promise(function (resolve, reject) {

        //console.log(result);

        //console.log(result);

        //var cw01 = _.filter(result, 'cw01');

        //var r = _.findKey(result, 'positive');

        console.log(result);

        var cw01 = result[0].cw01.positive.toString() + ", " + result[1].cw01.neutral.toString() + ", " + result[2].cw01.negative.toString();
        var cw09 = result[0].cw09.positive.toString() + ", " + result[1].cw09.neutral.toString() + ", " + result[2].cw09.negative.toString();
        var cw11 = result[0].cw11.positive.toString() + ", " + result[1].cw11.neutral.toString() + ", " + result[2].cw11.negative.toString();
        var cw70 = result[0].cw70.positive.toString() + ", " + result[1].cw70.neutral.toString() + ", " + result[2].cw70.negative.toString();

        var result2 = {

          cw01: cw01,
          cw09: cw09,
          cw11: cw11,
          cw70: cw70

        }

        //console.log(cw01);

        //var positive = _.mapValues(result, 'positive');
        //var negative = _.mapValues(result, 'negative');

        //console.log(positive);

        res.json(result2);

      })
    }

    A()
     .then(function (result) {
       return B(result);
     });

};

exports.write_blockchain_watson = function(req, res) {

  var discovery = new DiscoveryV1({
    //url: 'https://gateway.watsonplatform.net/discovery/api',
    username: '13195df6-810c-46e6-94a6-fcdc1c6e7d54',
    password: 'o8SP5rqW8kJo',
    version_date: DiscoveryV1.VERSION_DATE_2017_08_01
  });

  var A = function () {

      return new Promise(function (resolve, reject) {

        var options = {
          method: 'GET',
          url: 'http://localhost:3000/hyperledger/all'
        };

        request(options, function(error, response, body) {
          //if (error) throw new Error(error);
          if (error) reject(error);

          var hyperblock = JSON.parse(body).slice(1, 500);

          resolve(hyperblock);

        });

      })
    }

   var B = function (result) {

      return new Promise(function (resolve, reject) {

  var i = 0;

  function myLoop (result) {
     setTimeout(function () {

       var sentiment = null;
       var time = result[i].Record.ELAPSEDTIME

       console.log(time);

       switch(parseInt(time.substring(4,6)) > 0) {
            case true:
                sentiment = "disatisfied"
                break;

            case false:

            if(parseInt(time.substring(6,8)) <= 5){

              sentiment = "satisfied"

            }

            if(parseInt(time.substring(6,8)) > 5 && parseInt(time.substring(6,8)) <= 10 ){

              sentiment = "tolerating"

            }


            if(parseInt(time.substring(6,8)) > 10){

              sentiment = "disatisfied"

            }

                break;

            default:
                sentiment = null
  }

          //console.log(sentiment);

          result[i].Record.sentiment = sentiment;
            result[i].Record.timestamp = moment().utc().format("YYYY-MM-DDThh:mm:ss:SSSS");

          console.log(result[i].Record);

          //00.08.54.5243
          //1234567890123

       discovery.addJsonDocument(({
           environment_id: "f17611a6-ff04-464d-9792-73846a1a0d34",
           collection_id: "6a7ff7af-2122-400e-a0c3-427392db9e73",
           file: result[i]
         }))

        i++;
        if (i < 500) {
           myLoop(result);
        } else {resolve("Finished")}
     }, 2000)
  }

  myLoop(result);

      })
    }

    A()
     .then(function (result) {
       return B(result);
     });

  res.json("Blockchain hyperledger streaming to Watson AI.")

}
