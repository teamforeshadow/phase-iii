/*
____ ____ ____ ____ ____ _  _ ____ ___  ____ _ _ _
|___ |  | |__/ |___ [__  |__| |__| |  \ |  | | | |
|    |__| |  \ |___ ___] |  | |  | |__/ |__| |_|_|

*/

/*
Foreshadow Middleware API
Created by: Team Foreshadow - IBM's Unchain the Mainframe 2017
Contributors:
  David Kennedy,
  Michael Machnik,
  Andrea Saxman,
  Joseph Parisi,
  Daryl Stanton

Directions: forever start server.js from /opt

*/

'use strict';
module.exports = function(app) {
  var foreshadow = require('../controllers/foreshadowController');

  app.route('/tasks')
    .get(foreshadow.list_all_tasks)
    .post(foreshadow.create_a_task);

  app.route('/tasks/:taskId')
    .get(foreshadow.read_a_task)
    .put(foreshadow.update_a_task)
    .delete(foreshadow.delete_a_task);

  app.route('/hyperledger/:query')
    .get(foreshadow.read_hyperledger);

  app.route('/hyperledger/add/:system/:region/:zosserverid/:startime/:endtime/:type/:jobname/:uid')
    .get(foreshadow.write_hyperledger);

  app.route('/sync')
    .get(foreshadow.sync_hyperledger);

  app.route('/analytics')
    .get(foreshadow.hyperledger_analytics);

  app.route('/watson')
    .get(foreshadow.watson)
    .post(foreshadow.watson);

  app.route('/watson-relative/:interval')
    .get(foreshadow.watson_relative);

  app.route('/watson-sync')
    .get(foreshadow.write_blockchain_watson);

};
