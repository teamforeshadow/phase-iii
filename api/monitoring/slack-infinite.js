//Slack Notification for Foreshadow Watson based Machine Learning and Analytics

//Webhook URL
  //https://hooks.slack.com/services/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

//channel
    //mainframe-devops

var Slack = require('slack-node');
var _ = require('lodash');
var request = require('request');

var positive = new Promise((resolve, reject) => {

  var options = { method: 'GET',
url: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/f17611a6-ff04-464d-9792-73846a1a0d34/collections/636ffdcc-94f8-4752-b10c-8c143df671a0/query',
qs:
{ version: '2017-10-16',
 count: '',
 offset: '',
 aggregation: '',
 filter: 'opSystem::"PROD"',
 passages: 'true',
 deduplicate: 'false',
 highlight: 'true',
 return: '',
 query: 'enriched_sentiment.sentiment.document.label::"positive"' },
headers:
{ 'postman-token': 'bb097f8a-9529-65c6-446b-93767f738b92',
 'cache-control': 'no-cache',
 authorization: 'Basic MTMxOTVkZjYtODEwYy00NmU2LTk0YTYtZmNkYzFjNmU3ZDU0Om84U1A1cnFXOGtKbw==' } };

      request(options, function (error, response, body) {
      if (error) throw new Error(error);

      var json = JSON.parse(body);

          resolve(json.matching_results)

});

});

  var neutral = new Promise((resolve, reject) => {

        var options = { method: 'GET',
        url: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/f17611a6-ff04-464d-9792-73846a1a0d34/collections/636ffdcc-94f8-4752-b10c-8c143df671a0/query',
        qs:
        { version: '2017-10-16',
        count: '',
        offset: '',
        aggregation: '',
        filter: 'opSystem::"PROD"',
        passages: 'true',
        deduplicate: 'false',
        highlight: 'true',
        return: '',
        query: 'enriched_sentiment.sentiment.document.label::"neutral"' },
        headers:
        { 'postman-token': 'f2d29730-9c6b-8b19-9119-4adc134df1d8',
        'cache-control': 'no-cache',
        authorization: 'Basic MTMxOTVkZjYtODEwYy00NmU2LTk0YTYtZmNkYzFjNmU3ZDU0Om84U1A1cnFXOGtKbw==' } };

        request(options, function (error, response, body) {
        if (error) throw new Error(error);

        var json = JSON.parse(body);

            resolve(json.matching_results)

});


});

  var negative = new Promise((resolve, reject) => {

        var options = { method: 'GET',
        url: 'https://gateway.watsonplatform.net/discovery/api/v1/environments/f17611a6-ff04-464d-9792-73846a1a0d34/collections/636ffdcc-94f8-4752-b10c-8c143df671a0/query',
        qs:
        { version: '2017-10-16',
        count: '',
        offset: '',
        aggregation: '',
        filter: 'opSystem::"PROD"',
        passages: 'true',
        deduplicate: 'false',
        highlight: 'true',
        return: '',
        query: 'enriched_sentiment.sentiment.document.label::"negative"' },
        headers:
        { 'postman-token': 'f2d29730-9c6b-8b19-9119-4adc134df1d8',
        'cache-control': 'no-cache',
        authorization: 'Basic MTMxOTVkZjYtODEwYy00NmU2LTk0YTYtZmNkYzFjNmU3ZDU0Om84U1A1cnFXOGtKbw==' } };

        request(options, function (error, response, body) {
        if (error) throw new Error(error);

        var json = JSON.parse(body);

            resolve(json.matching_results)

});


});


var A = function () {

    return new Promise(function (resolve, reject) {


      Promise.all([positive, neutral, negative]).then(values => {

        function add(a, b) {
            return a + b;
        }

        var positive = values[0]
        var neutral = values[1]
        var negative = values[2]

        console.log(values);

    //Total number of records with sentiment
        var sum = [positive, neutral, negative].reduce(add, 0);
          console.log(sum);

         var pShare = (positive/sum);

    // //Neutral Percentage

        var neuShare = (neutral/sum);

    //Negative Percentage

         var negShare = (negative/sum);

      var sentimentPercent = {

        p: parseInt(pShare*100),
        neu: parseInt(neuShare*100),
        neg: parseInt(negShare*100)

      }

      resolve(sentimentPercent);

    });

    })
  }

 var B = function (result) {

    return new Promise(function (resolve, reject) {

      console.log(result)

    if(result.neg >= 10){

      var webhookUri = "https://hooks.slack.com/services/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";

      slack = new Slack();

      slack.setWebhook(webhookUri);

      var diff = result.neg - 10;

      slack.webhook({
        channel: "#mainframe-devops",
        username: "Watson",
        //text: "Hello! Attention! There seems to be an issue with CW01, and the CWXTCOB job. For the last 5 minutes, 100 users have been affected and are disatisfied with their experience. Please use the following URL understand more. Hopefully, this information doesn't ruin your day."
        text: "Attention! In the last *5 minutes*, there seems to be an increase in negative sentiment within your mainframe production environment. Current rate is *" + diff + " % greater* than acceptable levels."

      }, function(err, response) {
        console.log(response);
      });

        resolve("Sent message to Slack");

      } else {

        console.log("Sentiment within tolerance. No messages sent.")

      }

    })
  }

function monitoring(){

  A()
   .then(function (result) {
     return B(result);
   });

 }

//monitoring();

//Loop as a monitoring service
  setInterval(monitoring, 100000)
