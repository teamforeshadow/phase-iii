/*
____ ____ ____ ____ ____ _  _ ____ ___  ____ _ _ _
|___ |  | |__/ |___ [__  |__| |__| |  \ |  | | | |
|    |__| |  \ |___ ___] |  | |  | |__/ |__| |_|_|

*/

/*
Foreshadow Middleware API
Created by: Team Foreshadow - IBM's Unchain the Mainframe 2017
Contributors:
  David Kennedy,
  Michael Machnik,
  Andrea Saxman,
  Joseph Parisi,
  Daryl Stanton

Directions: forever start server.js from /opt

*/
/*
* Copyright IBM Corp All Rights Reserved
*
* SPDX-License-Identifier: Apache-2.0
*/
/*
 * Hyperledger Fabric Sample Query Program
 */

'use strict';

module.exports = function query(query) {

//function query () {

var hfc = require('fabric-client');
var path = require('path');
var _ = require('lodash');

var options = {
    wallet_path: "../hyperledger/fabric-samples/fabcar/creds",
    user_id: 'PeerAdmin',
    channel_id: 'mychannel',
    chaincode_id: 'fabcar',
    network_url: 'grpc://localhost:7051',
};

var channel = {};
var client = null;
var queryType = null;
var filter = null;
var filterKey = null;
var filterValue = null;

console.log("query:" + query);
console.log(process.argv);

if(query == "all"){

    queryType = 'queryAllCars';
    query = '';

} else {

    queryType = 'queryCar';
    query = query;

}

if (process.argv[4] == "filter"){

  var filterKey =  process.argv[5];
  var filterValue = process.argv[6];

  console.log("filterKey:" + filterKey);
  console.log("filterValue:" + filterValue);

}

Promise.resolve().then(() => {
    console.log("Create a client and set the wallet location");
    client = new hfc();
    return hfc.newDefaultKeyValueStore({ path: options.wallet_path });
}).then((wallet) => {
    console.log("Set wallet path, and associate user ", options.user_id, " with application");
    client.setStateStore(wallet);
    return client.getUserContext(options.user_id, true);
}).then((user) => {
    console.log("Check user is enrolled, and set a query URL in the network");
    if (user === undefined || user.isEnrolled() === false) {
        console.error("User not defined, or not enrolled - error");
    }
    channel = client.newChannel(options.channel_id);
    channel.addPeer(client.newPeer(options.network_url));
    return;
}).then(() => {
    console.log("Make query");
    var transaction_id = client.newTransactionID();
    console.log("Assigning transaction_id: ", transaction_id._transaction_id);

    // queryCar - requires 1 argument, ex: args: ['CAR4'],
    // queryAllCars - requires no arguments , ex: args: [''],


//Foreshadow query all SMF Records

/*
    const request = {
        chaincodeId: options.chaincode_id,
        txId: transaction_id,
        fcn: 'queryAllCars',
        args: ['']
    };

*/

//Foreshadow query 1 SMF record


    const request = {
        chaincodeId: options.chaincode_id,
        txId: transaction_id,
        fcn: queryType,
        args: [query]
    };


    return channel.queryByChaincode(request);
}).then((query_responses) => {
    console.log("returned from query");
    if (!query_responses.length) {
        console.log("No payloads were returned from query");
        //return ("No payloads were returned from query")
    } else {
        console.log("Query result count = ", query_responses.length)
        //return ("Query result count = ", query_responses.length)
    }
    if (query_responses[0] instanceof Error) {
        console.error("error from query = ", query_responses[0]);
        //return ("error from query = ", query_responses[0])
    }

    console.log(query_responses[0].toString());

    return query_responses[0].toString();

}).catch((err) => {
    console.error("Caught Error", err);
});

}
