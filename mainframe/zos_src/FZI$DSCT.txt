         MACRO                                                                  
         FZI$DSCT &ZOS=YES,                                            *        
               &PRINT=YES,                                             *        
               &SERVER=YES,                                            *        
               &SMF=NO,                                                *        
               &WEB=NO,                                                *        
               &CICS=NO,                                               *        
               &PLIST=YES                                                       
***********************************************************************         
**                                                                   **         
**                                                                   **         
**        FZI$DSCT - ALL DSECTS FOR THE FORESHADOW Z/OS INTERFACE.   **         
**                                                                   **         
**                                                                   **         
**                                                                   **         
**  CHANGE CONTROL                                                   **         
**  MEMBER ADDED                                     MAM 08/15/17   #00         
**                                                                   **         
***********************************************************************         
         PRINT OFF                                                              
***********************************************************************         
**                                                                   **         
**       BASE MACROS (REQUIRED).                                     **         
**                                                                   **         
***********************************************************************         
         TITLE '-- FZI -- REGISTER EQUATES'                                     
*        FZI$REGS                                                               
         FZI$REGS                                                               
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- RELATIVE BRANCH OPSYNS'                               
*        COPY  IEABRC                                                           
         COPY  IEABRC                                                           
         EJECT                                                                  
.*                                                                              
         PRINT ON                                                               
         AIF   ('&PRINT' NE 'OFF').S100                                         
         PRINT OFF                                                              
.S100    ANOP                                                                   
         AIF   ('&SERVER' NE 'YES').P100                                        
***********************************************************************         
**                                                                   **         
**       SERVER MACROS.                                              **         
**                                                                   **         
***********************************************************************         
         TITLE '-- FZI -- GLOBAL VECTOR TABLE  (GVCT)'                          
*        FZI$GVCT DSECT=YES,LABL=GVCT                                           
         FZI$GVCT DSECT=YES,LABL=GVCT                                           
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- SUBTASK CONTROL BLOCK (STSK)'                         
*        FZI$STSK DSECT=YES,LABL=STSK                                           
         FZI$STSK DSECT=YES,LABL=STSK                                           
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- WORK REQUEST ELEMENT (WRE)'                           
*        FZI$WRE  DSECT=YES,LABL=WRE                                            
         FZI$WRE  DSECT=YES,LABL=WRE                                            
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- DISPATCHER WORK REQUEST ELEMENT (DWRE)'               
*        FZI$DWRE DSECT=YES,LABL=DWRE                                           
         FZI$DWRE DSECT=YES,LABL=DWRE                                           
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- REST WORK REQUEST ELEMENT (RWRE)'                     
*        FZI$RWRE DSECT=YES,LABL=RWRE                                           
         FZI$RWRE DSECT=YES,LABL=RWRE                                           
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- LOGGER WORK REQUEST ELEMENT (LWRE)'                   
*        FZI$LWRE DSECT=YES,LABL=LWRE                                           
         FZI$LWRE DSECT=YES,LABL=LWRE                                           
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- CONFIGURATION BLOCK  (CNFG)'                          
*        FZI$CNFG DSECT=YES,LABL=CNFG                                           
         FZI$CNFG DSECT=YES,LABL=CNFG                                           
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- JOURNAL/MSG CONTROL BLOCK  (LOGA)'                    
*        FZI$LOG  DSECT=YES,LABL=LOG                                            
         FZI$LOG  DSECT=YES,LABL=LOG                                            
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- FUNCTIONS MODULE: STCK'                               
*        FZI$STCK DSECT=YES,LABL=STCK                                           
         FZI$STCK DSECT=YES,LABL=STCK                                           
         EJECT                                                                  
.*                                                                              
.P100    ANOP                                                                   
         AIF   ('&PLIST' NE 'YES').I100                                         
***********************************************************************         
**                                                                   **         
**       PARAMETER LIST MACROS.                                      **         
**                                                                   **         
***********************************************************************         
         AIF   ('&WEB' NE 'YES').P120                                           
         TITLE '-- FZI -- HTTP SERVICE ROUTINE PARAMETER LIST (HTPL)'           
*        FZI$HTPL DSECT=YES,LABL=HTPL                                           
         FZI$HTPL DSECT=YES,LABL=HTPL                                           
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- SEND PARAMETER LIST (SEND)'                           
*        FZI$SEND DSECT=YES,LABL=SEND                                           
         FZI$SEND DSECT=YES,LABL=SEND                                           
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- JSON SERVICE ROUTINE PARAMETER LIST (JSPL)'           
*        FZI$JSPL DSECT=YES,LABL=JSPL                                           
         FZI$JSPL DSECT=YES,LABL=JSPL                                           
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- JSON / SMF TABLE CREATION (JSMF)'                     
*        FZI$JSMF DSECT,LABL=JSMF,PRINT=YES                                     
         FZI$JSMF DSECT,LABL=JSMF,PRINT=YES                                     
         EJECT                                                                  
.*                                                                              
.P120    ANOP                                                                   
         AIF   ('&SMF' NE 'YES').I100                                           
         TITLE '-- FZI -- REAL-TIME SMF PARAMETER LIST (SLPL)'                  
*        FZI$SLPL DSECT=YES,LABL=SLPL                                           
         FZI$SLPL DSECT=YES,LABL=SLPL                                           
         EJECT                                                                  
.*                                                                              
         TITLE '-- FZI -- SMF HEADER RECORD'                                    
*        FZI$SMF  DSECT=YES,LABL=SMF                                            
         FZI$SMF  DSECT=YES,LABL=SMF                                            
         EJECT                                                                  
.*                                                                              
.I100    ANOP                                                                   
         AIF   ('&ZOS' NE 'YES').I120                                           
***********************************************************************         
**                                                                   **         
**       IBM MACROS.                                                 **         
**                                                                   **         
***********************************************************************         
         TITLE '-- Z/OS -- SYSTEM MACROS'                                       
*        CVT DSECT=YES,LIST=YES                                                 
         CVT DSECT=YES,LIST=YES                                                 
         EJECT                                                                  
.*                                                                              
*        IHAECVT DSECT=YES,LIST=YES                                             
         IHAECVT DSECT=YES,LIST=YES                                             
         EJECT                                                                  
.*                                                                              
*        IHAPSA                                                                 
         IHAPSA                                                                 
         EJECT                                                                  
.*                                                                              
*        IEANTASM                                                               
         IEANTASM                                                               
         EJECT                                                                  
.*                                                                              
*        IHASDWA DSECT=YES                                                      
         IHASDWA DSECT=YES                                                      
         EJECT                                                                  
.*                                                                              
*        IHASTCB                                                                
         IHASTCB                                                                
         EJECT                                                                  
.*                                                                              
*        IKJTCB  LIST=YES                                                       
         IKJTCB  LIST=YES                                                       
         EJECT                                                                  
.*                                                                              
*        IHAASCB DSECT=YES,LIST=YES                                             
         IHAASCB DSECT=YES,LIST=YES                                             
         EJECT                                                                  
.*                                                                              
*        DCBD    DSORG=(QS)                                                     
         DCBD    DSORG=(QS)                                                     
         EJECT                                                                  
.*                                                                              
*COM     DSECT                                                                  
*        IEZCOM                                                                 
COM      DSECT                                                                  
         IEZCOM                                                                 
         EJECT                                                                  
.*                                                                              
*CIB     DSECT                                                                  
*        IEZCIB                                                                 
CIB      DSECT                                                                  
         IEZCIB                                                                 
         EJECT                                                                  
.*                                                                              
*        IXGENF  DSECT=YES,LIST=YES                                             
         IXGENF  DSECT=YES,LIST=YES                                             
         EJECT                                                                  
.*                                                                              
***********************************************************************         
**                                                                   **         
**       UNIX SYSTEM SERVICES.                                       **         
**                                                                   **         
***********************************************************************         
*        BPXYOEXT                                                               
         BPXYOEXT                                                               
         EJECT                                                                  
.*                                                                              
*        BPXYCONS                                                               
         BPXYCONS                                                               
         EJECT                                                                  
.*                                                                              
.I120    ANOP                                                                   
         AIF   ('&SMF' NE 'YES').I140                                           
         TITLE '-- SMF -- SMF DATA AREAS'                                       
***********************************************************************         
**                                                                   **         
**       SMF DATA AREAS.                                             **         
**                                                                   **         
***********************************************************************         
*        IFAZSYSP DSECT=YES                                                     
         IFAZSYSP DSECT=YES                                                     
         EJECT                                                                  
.*                                                                              
*        IFARCINM LIST=YES                                                      
         IFARCINM LIST=YES                                                      
         EJECT                                                                  
.*                                                                              
*        IEESMCA                                                                
         IEESMCA                                                                
         EJECT                                                                  
.*                                                                              
*        IFASMFR 23                                                             
         IFASMFR 23                                                             
         EJECT                                                                  
.*                                                                              
*        IFASMFR 30                                                             
         IFASMFR 30                                                             
         EJECT                                                                  
.*                                                                              
*        DSNDQWAS DSECT=YES,SUBTYPE=ALL (101)                                   
*        DSNDQWAS DSECT=YES,SUBTYPE=ALL                                         
         EJECT                                                                  
.*                                                                              
*        DSNDQWSP DSECT=YES,SUBTYPE=ALL (102)                                   
*        DSNDQWSP DSECT=YES,SUBTYPE=ALL                                         
         EJECT                                                                  
.*                                                                              
***********************************************************************         
**                                                                   **         
**       CICS.                                                       **         
**                                                                   **         
***********************************************************************         
         TITLE '-- CICS -- SMF HEADER'                                          
*        COPY     DFHSMFDS                                                      
         COPY     DFHSMFDS                                                      
DFHSMFDL EQU      *-DFHSMFDS                                                    
         EJECT                                                                  
.*                                                                              
         TITLE '-- CICS -- MONITORING RECORD'                                   
*        COPY  DFHMNTDS (110 - MONITORING)                                      
         COPY  DFHMNTDS                                                         
.*                                                                              
         EJECT                                                                  
         TITLE '-- CICS -- DICTIONARY ENTRY'                                    
*DICTNTRY DSECT                                                                 
*         DFHMCTDR TYPE=(PREFIX,CMO)                                            
DICTNTRY DSECT                                                                  
         DFHMCTDR TYPE=(PREFIX,CMO)                                             
         EJECT                                                                  
.*                                                                              
         TITLE '-- CICS -- EXCEPTION ENTRY'                                     
*        DFHMNEXC PREFIX=EXC                                                    
         DFHMNEXC PREFIX=EXC                                                    
MNEXCDSL EQU      *-MNEXCDS                                                     
         EJECT                                                                  
.*                                                                              
         TITLE '-- CICS -- RESOURCE RECORD'                                     
*        COPY  DFHMNRDS                                                         
         COPY  DFHMNRDS                                                         
DFHMNRDL EQU      *-DFHMNRDS                                                    
         EJECT                                                                  
.*                                                                              
         TITLE '-- CICS -- IDENTITY RECORD (UNUSED)'                            
*        COPY  DFHMNIDS                                                         
         EJECT                                                                  
.*                                                                              
 AGO .SKIP_CICS1 -------------------------------                                
*        DFHMNSMF (110)                                                         
         DFHMNSMF                                                               
SMFMNDLN EQU   *-MNSMFDS               LENGTH-MONITORING HEADER                 
         EJECT                                                                  
.*                                                                              
*        COPY  DFHSMSDS (110 - MONITORING SUBTYPE)                              
*        COPY  DFHMNCBD                                                         
         EJECT                                                                  
.SKIP_CICS1 ANOP ---------------------------------                              
.*                                                                              
*        ERBSMF70                                                               
.*       ERBSMF70                                                               
         EJECT                                                                  
.*                                                                              
.I140    ANOP                                                                   
         AIF   ('&WEB' NE 'YES').Z100                                           
         TITLE '-- Z/OS -- WEB ENABLEMENT'                                      
***********************************************************************         
**                                                                   **         
**       WEB ENABLEMENT MACROS.                                      **         
**                                                                   **         
***********************************************************************         
*HWTH    DSECT                                                                  
*        HWTHIASM                                                               
HWTH     DSECT                                                                  
         HWTHIASM                                                               
         EJECT                                                                  
*        HWTHKASM                                                               
         HWTHKASM                                                               
         EJECT                                                                  
.*                                                                              
*HWTJ    DSECT                                                                  
*        HWTJIASM                                                               
HWTJ     DSECT                                                                  
         HWTJIASM                                                               
         EJECT                                                                  
*        HWTJKASM                                                               
         HWTJKASM                                                               
         EJECT                                                                  
.*                                                                              
.Z100    ANOP                                                                   
         AIF   ('&PRINT' NE 'OFF').Z120                                         
         PRINT OFF                                                              
.Z120    ANOP                                                                   
.*                                                                              
         MEND                                                                   
