         PUNCH '  MODE AMODE(31),RMODE(ANY) '                                   
         PUNCH '  SETCODE AC(1)             '                                   
         PUNCH '  ENTRY FZIMMAIN            '                                   
         EJECT                                                                  
&NTPN    SETC  'FZIGVCT.CMN.'                                                   
FZIMMAIN TITLE '-- FZI SERVER MAINLINE MODULE'                                  
***********************************************************************         
**                                                                   **         
**                                                                   **         
**       FZIMMAIN - FORSHADOW Z/OS INTERFACE MAINLINE.               **         
**                                                                   **         
**                                                                   **         
**                                                                   **         
**  CHANGE CONTROL                                                   **         
**  MEMBER ADDED                                     MAM 08/15/17   @00         
**                                                                   **         
***********************************************************************         
FZIMMAIN FZI$ENTR TYPE=CSECT,                                          *        
               AMODE=31,                                               *        
               RMODE=ANY,                                              *        
               LOC=ANY                                                          
         L     R1,0(,R1)               LOAD PTR -> PARM LENGTH                  
         LH    R2,0(,R1)               LOAD LENGTH-PARM                         
         CH    R2,=H'4'                IS LENGTH 4 BYTES?                       
         BNE   HSKP@040                NO  - ABEND                              
         MVC   $FZIID,2(R1)            INIT ENVIRONMENT_ID                      
         B     MAIN                    AND CONTINUE                             
HSKP@040 DS    0H                                                               
         MVC   $FZIID,=CL4'FZID'       INIT DEFAULT ENVIRONMENT_ID              
*        DC    H'0'                    INVALID PARM, ABEND                      
***********************************************************************         
**                                                                   **         
**       MAINLINE.                                                   **         
**                                                                   **         
***********************************************************************         
MAIN     DS    0H                                                               
         MODESET KEY=ZERO,MODE=SUP     GET INTO KEY 0 SUP STATE                 
***********************************************************************         
**                                                                   **         
**       PERFORM INITIALIZATION ROUTINE.                             **         
**                                                                   **         
***********************************************************************         
         L     R10,#INIT@              LOAD PTR -> INIT S/R                     
         BASR  R11,R10                 CALL ROUTINE                             
         LTR   R15,R15                 EVERYTHING OKAY?                         
         BNZ   MAIN@900                NO - EXIT                                
         L     R9,$GVCT@               LOAD PTR -> GLOBAL AREA                  
         USING GVCT,R9                 ESTABLISH ADDRESSABILITY                 
***********************************************************************         
**                                                                   **         
**       ESTABLISH RECOVERY ENVIRONMENT.                             **         
**                                                                   **         
***********************************************************************         
         ST    R12,$WKESTPB            SAVE PTR -> PGM BASE FOR ESTAE           
         ST    R13,$WKESTWA            SAVE PTR -> PGM DSA  FOR ESTAE           
         ST    R9,$WKESTVT             SAVE PTR -> PGM GVCT FOR ESTAE           
         LA    R1,MAIN@600             LOAD PTR -> RETRY ROUTINE                
         ST    R1,$WKESTRR             SAVE PTR -> RETRY ROUTINE                
         MVC   $ESTAE,#ESTAEX          INIT LIST FORM ESTAEX                    
         LLGF  R2,#ESTA@               LOAD PTR -> ESTAE ROUTINE                
         ESTAEX (R2),                  ISSUE ESTAEX                    *        
               PARAM=$WKESTWA,                                         *        
               TERM=YES,                                               *        
               MF=(E,$ESTAE)                                                    
***********************************************************************         
**                                                                   **         
**       INDICATE THE SERVER IS UP AND ACTIVE.                       **         
**                                                                   **         
***********************************************************************         
         OI    GVCTSRF1,GVCT@ACT       TURN ON ACTIVE FLAG                      
***********************************************************************         
**                                                                   **         
**       SUSPEND THE ADDRESS SPACE.                                  **         
**                                                                   **         
***********************************************************************         
         L     R10,#WRTN@              LOAD PTR -> INIT S/R                     
         BASR  R11,R10                 CALL ROUTINE                             
         LTR   R15,R15                 EVERYTHING OKAY?                         
         BNZ   MAIN@900                NO - EXIT                                
***********************************************************************         
**                                                                   **         
**       CLEANUP ROUTINE.                                            **         
**                                                                   **         
***********************************************************************         
         L     R10,#CLNR@              LOAD PTR -> CLNR S/R                     
         BASR  R11,R10                 CALL ROUTINE                             
         LTR   R15,R15                 EVERYTHING OKAY?                         
         BNZ   MAIN@900                NO - EXIT                                
***********************************************************************         
**                                                                   **         
**       MAINLINE RECOVERY ROUTINE.                                  **         
**                                                                   **         
***********************************************************************         
MAIN@600 DS    0H                                                               
         WTO   'FZIMMAIN ABEND OCCURRED..'                                      
         LA    R15,16                  LOAD R/C = 16                            
         B     MAIN@900                EVERYTHING OK                            
***********************************************************************         
**                                                                   **         
**       MAINLINE IMMEDIATE TERMINATION ROUTINES.                    **         
**                                                                   **         
***********************************************************************         
MAIN@700 DS    0H                                                               
         LA    R15,0                   LOAD R/C = 0                             
         B     MAIN@900                EVERYTHING OK                            
***********************************************************************         
**                                                                   **         
**       MAINLINE ERROR ROUTINES.                                    **         
**                                                                   **         
***********************************************************************         
MAIN@800 DS    0H                                                               
         LA    R15,8                   LOAD R/C = 8                             
         B     MAIN@900                ERROR                                    
***********************************************************************         
**                                                                   **         
**       PROGRAM EXIT LINKAGE.                                       **         
**                                                                   **         
***********************************************************************         
MAIN@900 DS    0H                                                               
         ST    R15,$RC                 SAVE OFF R/C                             
         ESTAEX 0                      CANCEL ESTAEX                            
         SPACE                                                                  
         MODESET KEY=NZERO,MODE=PROB   BACK TO PROBLEM STATE                    
         SPACE                                                                  
         FZI$EXIT RC=$RC               RETURN TO CALLER                         
         EJECT                                                                  
***********************************************************************         
**                                                                   **         
**       PROGRAM CONSTANTS.                                          **         
**                                                                   **         
***********************************************************************         
#DUBR@   DC    A(DUBR)                 PTR -> DUBR S/R                          
#LODR@   DC    A(LODR)                 PTR -> LODR S/R                          
#WRTN@   DC    A(WRTN)                 PTR -> WRTN S/R                          
#TIMX@   DC    A(TIMX)                 PTR -> TIMX S/R                          
#INIT@   DC    A(INIT)                 PTR -> INIT S/R                          
#CLNR@   DC    A(CLNR)                 PTR -> CLNR S/R                          
#CNSI@   DC    A(CNSI)                 PTR -> CNSI S/R                          
#ATTC@   DC    A(ATTC)                 PTR -> ATTC S/R                          
#DETC@   DC    A(DETC)                 PTR -> DETC S/R                          
#ESTA@   DC    A(ESTA)                 PTR -> ESTA S/R                          
#GOBJ@   DC    A(GOBJ)                 PTR -> GOBJ S/R                          
                                                                                
#ECB@PST EQU   X'40'                                                            
#WAITIVL DC    F'1000'                 WAIT INTERVAL                            
#NTPSYSL DC    A(IEANT_SYSTEM_LEVEL)                                            
#NTPPERS DC    A(IEANT_PERSIST)                                                 
                                                                                
#ENQ     ENQ   (,,E,,SYSTEM),          ENQ                             *        
               RET=USE,                                                *        
               MF=L                                                             
#ENQL    EQU   *-#ENQ                                                           
#STIMRM  STIMERM SET,                  STIMERM SET                     *        
               MF=L                                                             
#STIMRML EQU   *-#STIMRM                                                        
#STIMRC  STIMERM CANCEL,               STIMERM CANCEL                  *        
               MF=L                                                             
#STIMRCL EQU   *-#STIMRM                                                        
#SDUMP   SDUMPX PLISTVER=3,            SDUMPX                          *        
               MF=L                                                             
#SDUMPL  EQU   *-#SDUMP                                                         
#ATTCH   ATTACH EP=ZZZZZZZZ,           ATTACH                          *        
               ECB=*,                                                  *        
               SF=L                                                             
#ATTCHL  EQU   *-#ATTCH                                                         
#ESTAEX  ESTAEX *,                     ESTAEX                          *        
               PARAM=*,                                                *        
               TERM=YES,                                               *        
               MF=L                                                             
#ESTAEXL EQU   *-#ESTAEX                                                        
         EJECT                                                                  
***********************************************************************         
**                                                                   **         
**       PROGRAM LITERAL POOL.                                       **         
**                                                                   **         
***********************************************************************         
         LTORG                                                                  
         EJECT                                                                  
         PUSH  USING                                                            
***********************************************************************         
**                                                                   **         
**       INITIALIZATION ROUTINE.                                     **         
**                                                                   **         
***********************************************************************         
INIT     DS    0H                                                               
         USING INIT,R10                                                         
         STM   R0,R15,$LNKINIT         SAVE CALLER'S REGISTERS                  
***********************************************************************         
**                                                                   **         
**       ATTEMPT TO ENQ ON THE GLOBAL AREA NTP.                      **         
**                                                                   **         
***********************************************************************         
         MVI   $NTPNAME,X'40'          INIT NTP NAME TO SPACES                  
         MVC   $NTPNAME+1(L'$NTPNAME-1),$NTPNAME                                
         MVC   $NTPTOKN,=XL16'00'      INIT NTP TOKEN TO ZEROS                  
         MVC   $NTPFIXD,=CL12'&NTPN'   INIT FIXED PORTION OF NAME               
         MVC   $NTPFZID,$FZIID         INIT ENVIRONMENT_ID                      
         SPACE                                                                  
         MVC   $ENQ,#ENQ               INIT LIST FORM ENQ                       
         ENQ   (=CL8'FZIMGVCT',                                        *        
               $NTPNAME,                                               *        
               E,                                                      *        
               L'$NTPNAME,                                             *        
               SYSTEM),                                                *        
               RET=USE,                                                *        
               MF=(E,$ENQ)                                                      
         LTR   R15,R15                 DID WE GET ENQ?                          
         BNZ   INIT@800                NO - EXIT...ENQ ALREADY EXISTS           
***********************************************************************         
**                                                                   **         
**       LOCATE THE GLOBAL AREA.                                     **         
**                                                                   **         
***********************************************************************         
         L     15,X'10'                                                         
         L     15,X'220'(,15)                                                   
         L     15,X'14'(,15)                                                    
         L     15,X'08'(,15)           LOAD PTR -> NTP RETRIEVE RTN             
         CALL  (15),                   RETRIEVE NTP                    *        
               (#NTPSYSL,                                              *        
               $NTPNAME,                                               *        
               $NTPTOKN,                                               *        
               $NTPRC),                                                *        
               MF=(E,$NTCALL)                                                   
         LTR   R15,R15                 DID WE GET NTP?                          
         BZ    INIT@100                YES - USE IT..                           
***********************************************************************         
**                                                                   **         
**       OBTAIN A NEW GLOBAL AREA.                                   **         
**                                                                   **         
***********************************************************************         
         WTO   'FZIMMAIN GETTING A NEW GLOBAL AREA'                             
         STORAGE OBTAIN,                                               *        
               LENGTH=GVCTLEN,                                         *        
               BNDRY=PAGE,                                             *        
               LOC=ANY,                                                *        
               SP=241,                                                 *        
               KEY=0,                                                  *        
               COND=YES                                                         
         LTR   R15,R15                 WAS STORAGE OBTAINED?                    
         BNZ   INIT@810                NO - EXIT S/R                            
         SPACE                                                                  
         ST    R1,$GVCT@               SAVE PTR -> GLOBAL AREA                  
         ST    R1,$NTPPTM@             SAVE PTR -> GVCT FOR NTP                 
         L     15,X'10'                                                         
         L     15,X'220'(,15)                                                   
         L     15,X'14'(,15)                                                    
         L     15,X'04'(,15)           LOAD PTR -> NTP CREATE RTN               
         SPACE                                                                  
         CALL  (15),                   CREATE NTP                      *        
               (#NTPSYSL,                                              *        
               $NTPNAME,                                               *        
               $NTPTOKN,                                               *        
               #NTPPERS,                                               *        
               $NTPRC),                                                *        
               MF=(E,$NTCALL)                                                   
         LTR   R15,R15                 WAS NAME/TOKEN PAIR CREATED?             
         BNZ   INIT@820                NO - SET R/C AND EXIT                    
***********************************************************************         
**                                                                   **         
**       NOW THAT WE HAVE A GLOBAL AREA, LOAD THE CONSTANTS          **         
**       MODULE AND INITIALIZE THE STORAGE.                          **         
**                                                                   **         
***********************************************************************         
INIT@100 DS    0H                                                               
         WTO   'FZIMMAIN FOUND EXISTING GLOBAL AREA *ONLY*'                     
         MVC   $EPNAME,=CL8'FZIMGVCT'  INIT MODULE NAME                         
         LOAD  EPLOC=$EPNAME,                                          *        
               ERRET=INIT@850                                                   
         N     R0,=X'7FFFFFFF'         ENSURE HIGH ORDER BIT 0                  
         ST    R0,$EP@                 COPY PTR -> EP                           
         N     R1,=X'00FFFFFF'         KEEP MOD SIZE IN DOUBLEWORD(S)           
         SLA   R1,3                    COMPUTE SIZE IN BYTES                    
         ST    R1,$EPL                 SAVE LENGTH-MODULE                       
                                                                                
         L     R1,$NTPPTM@             LOAD PTR -> GVCT                         
         ST    R1,$GVCT@               SAVE PTR -> GVCT                         
         LA    R0,GVCTDYN-GVCT(,R1)    LOAD PTR -> GVCTDYN                      
         L     R1,=A(GVCTLEN)          LOAD LENGTH-GVCT                         
         S     R1,=A(GVCTDYN-GVCT)     CALC LENGTH-DYNAMIC GVCT                 
                                                                                
         L     R14,$EP@                LOAD PTR -> CONSTANTS                    
         LA    R14,GVCTDYN-GVCT(,R14)  INCREMENT TO DYNAMIC PORTION             
         LR    R15,R1                  MOVE LENGTH-R1                           
         MVCL  R0,R14                  CLEAR GVCT STORAGE                       
         DELETE EPLOC=$EPNAME                                                   
***********************************************************************         
**                                                                   **         
**       INITIALIZE FIELDS IN THE GLOBAL AREA.                       **         
**                                                                   **         
***********************************************************************         
INIT@300 DS    0H                                                               
         L     R9,$GVCT@               COPY PTR -> GLOBAL AREA                  
         MVC   GVCTID,=CL4'GVCT'       MOVE BLOCK ID                            
         MVC   GVCTFZID,$FZIID         MOVE ENVIRONMENT_ID                      
         SPACE                                                                  
         STCK  GVCTTIME                SAVE INITIALIZATION TIME                 
         SPACE                                                                  
         ALESERV EXTRACTH,             OBTAIN STOKEN                   *        
               STOKEN=GVCTSTKN                                                  
         SPACE                                                                  
         TCBTOKEN TYPE=CURRENT,        GET OUR A/S TOKEN               *        
               TTOKEN=GVCTTTKN,                                        *        
               MF=(E,$TCBTKN)                                                   
         LTR   R15,R15                 DID WE GET IT?                           
         BNZ   INIT@830                NO - EXIT S/R                            
         SPACE                                                                  
         USING PSA,R0                  ESTABLISH ADDRESSABILITY                 
         MVC   GVCTASC@,PSAAOLD        MOVE PTR -> OUR ASCB                     
         DROP  R0                      DROP PSA                                 
         L     R1,CVTPTR               LOAD PTR -> CVT                          
         USING CVT,R1                  ESTABLISH ADDRESSABILITY                 
         MVC   GVCTSNAM,CVTSNAME       MOVE CURRENT SYSNAME                     
         DROP  R1                                                               
         SPACE                                                                  
         L     R1,GVCTASC@             LOAD PTR -> SERVER ASCB                  
         USING ASCB,R1                 ESTABLISH ADDRESSABILITY                 
         ICM   R14,15,ASCBJBNI         LOAD PTR -> OUR JOBNAME                  
         BNZ   INIT@320                AND IF IT'S ZERO                         
         L     R1,ASCBJBNS             LOAD PTR -> STC/TSO NAME                 
         SPACE                                                                  
INIT@320 DS    0H                                                               
         MVC   GVCTJOBN,0(R14)         INIT OUR JOBNAME                         
         DROP  R1                                                               
***********************************************************************         
**                                                                   **         
**       CALL THE MODULE LOAD ROUTINE.                               **         
**                                                                   **         
***********************************************************************         
         L     R10,#LODR@              LOAD PTR -> LODR S/R                     
         BASR  R11,R10                 CALL ROUTINE                             
         L     R10,#INIT@              -- RELOAD S/R BASE                       
         LTR   R15,R15                 EVERYTHING OKAY?                         
         BNZ   INIT@850                NO - EXIT ROUTINE                        
***********************************************************************         
**                                                                   **         
**       CALL THE CONFIGURATION ROUTINE.                             **         
**                                                                   **         
***********************************************************************         
         L     R15,GVCTCNF@            LOAD PTR -> CONFIGURATION RTN            
         BASR  R14,R15                 CALL ROUTINE                             
         LTR   R15,R15                 EVERYTHING OKAY?                         
         BNZ   INIT@840                NO - EXIT ROUTINE                        
***********************************************************************         
**                                                                   **         
**       CALL THE OBTAIN MEMORY OBJECT ROUTINE.                      **         
**                                                                   **         
