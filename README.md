#UX#

The Foreshadow UX is implemented with Vue.js for a modern and responsive frontend user experience. Scripts in the head of /demo/foreshadow-data.html interface with Foreshadow's Restful API to obtain records from the Foreshadow Hyperledger.

#API and Mainframe#

The repository contains the NodeJS (ExpressJS) logic and Mainframe zOS source. Foreshadow streams SMF records from a Mainframe CICS region and stores them downstream in a Blockchain hyperledger.

*Code of importance*

**mainframe / ***

**api / controllers / ***

**api / models / ***

**api / routes / ***

#Hyperledger#
This repository contains the Foreshadow chaincode that is initilized within the Blockchain Hyperledger.
Purpose: The functions in the chaincode create and help manage the Hyperledger.

*Defines SMF Struct
*Routes to handeler functions (queryAllSMF, querySMF, createSMF)
*Initialization of first 10 SMF Hyperledger records.
