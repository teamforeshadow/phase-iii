/*
____ ____ ____ ____ ____ _  _ ____ ___  ____ _ _ _
|___ |  | |__/ |___ [__  |__| |__| |  \ |  | | | |
|    |__| |  \ |___ ___] |  | |  | |__/ |__| |_|_|

*/

/*
Foreshadow Middleware API
Created by: Team Foreshadow - IBM's Unchain the Mainframe 2017
Contributors:
  David Kennedy,
  Michael Machnik,
  Andrea Saxman,
  Joseph Parisi,
  Daryl Stanton

Directions: forever start server.js from /opt

*/

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Task = require('./api/models/foreshadowModel'), //created model loading here
  bodyParser = require('body-parser');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/foreshadow');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/foreshadowRoute'); //importing route
var monitoring = require('./api/monitoring/slack-infinite'); //importing messaging and alerting module

routes(app); //register the route

app.listen(port);

console.log('Foreshadow SMF RESTful API server started on: ' + port);
