/*
____ ____ ____ ____ ____ _  _ ____ ___  ____ _ _ _
|___ |  | |__/ |___ [__  |__| |__| |  \ |  | | | |
|    |__| |  \ |___ ___] |  | |  | |__/ |__| |_|_|

*/

/*
Foreshadow Hyperledger Blockchain
Created by: Team Foreshadow - IBM's Unchain the Mainframe 2017
Contributors:
  David Kennedy,
  Michael Machnik,
  Andrea Saxman,
  Joeseph Perosi,
  Daryl Stanton

Purpose: The functions below create and help manage the Hyperledger
  1. Defines SMF Struct
  2. Routes to handeler functions (queryAllSMF, querySMF, createSMF)
  3. Initialization of first 10 SMF Hyperledger records

*/

package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

//Smart Contract structure

type SmartContract struct {
}

//Foreshadow
//Define the structure of the inbound SMF record

/* Mock SMF Mainframe Record

VALID JSON (RFC 4627)
Formatted JSON Data
{
	 "_id":"59d42f823c1df806035e5e09",
	 "RECORD":{
			"SYSTEM":"CW09",
			"REGION":"PMIMAMFD",
			"ZOSSERVERID":"FZID",
			"ID":"0001",
			"STARTTIME":"09.53.06.1236",
			"ENDTIME":"09:54:25.1534",
			"TYPE":"WITHDRAWL",
			"JOBNAME":"ZZZZZZZZ",
			"UID":"PMIMAM0"
	 },
	 "__v":0,
	 "status":[
			"pending"
	 ],
	 "Created_date":"2017-10-04T00:46:58.358Z"
}

*/

// type SMF struct {
// 	SYSTEM string `json:"SYSTEM"`
// 	REGION string `json:"REGION"`
// 	ZOSSERVERID string `json:"ZOSSERVERID"`
// 	STARTTIME string `json:"STARTTIME"`
// 	ENDTIME string `json:"ENDTIME"`
// 	TYPE string `json:"TYPE"`
// 	JOBNAME string `json:"JOBNAME"`
// 	UID string `json:"UID"`
// }

// args: [
// 	smfID,
// 	smf.SRVR_JOBNAME,
// 	smf.SRVR_ID,
// 	smf.SRVR_SYSTEM,
// 	smf.PRODNAME,
// 	smf.APPLID,
// 	smf.JOBNAME,
// 	smf.TRAN,
// 	smf.PROGRAMNAME,
// 	smf.ORIGINATINGUSER,
// 	smf.USERID,
// 	smf.STARTTIME,
// 	smf.ELAPSEDTIME
// ],



type SMF struct {
	SRVR_JOBNAME string `json:"SRVR_JOBNAME"`
	SRVR_ID string `json:"SRVR_ID"`
	SRVR_SYSTEM string `json:"SRVR_SYSTEM"`
	PRODNAME string `json:"PRODNAME"`
	APPLID string `json:"APPLID"`
	JOBNAME string `json:"JOBNAME"`
	TRAN string `json:"TRAN"`
	PROGRAMNAME string `json:"PROGRAMNAME"`
	ORIGINATINGUSER string `json:"ORIGINATINGUSER"`
	USERID string `json:"USERID"`
	STARTTIME string `json:"STARTTIME"`
	ELAPSEDTIME string `json:"ELAPSEDTIME"`
}


/*
 * The Init method is called when the Smart Contract "fabcar" is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 */
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract.
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately

	if function == "querySMF" {
		return s.querySMF(APIstub, args)
	} else if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "createSMF" {
		return s.createSMF(APIstub, args)
	} else if function == "queryAllSMF" {
		return s.queryAllSMF(APIstub)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) querySMF(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	carAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(carAsBytes)
}

//Foreshadow mods

	func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
		// smfs := []SMF{
		// 	SMF{SYSTEM: "CW09", REGION: "CICSR1", ZOSSERVERID: "FZID", STARTTIME: "09.53.06.1236", ENDTIME: "09:54:25.1534", TYPE: "WITHDRAW", JOBNAME: "BANK", UID: "PMIMAM0"},
		// 	SMF{SYSTEM: "CW09", REGION: "CICSR1", ZOSSERVERID: "FZID", STARTTIME: "09.54.06.1236", ENDTIME: "09:55:25.1534", TYPE: "DEPOSIT", JOBNAME: "BANK", UID: "PMIMAM0"},
		// 	SMF{SYSTEM: "CW09", REGION: "CICSR1", ZOSSERVERID: "FZID", STARTTIME: "09.55.06.1236", ENDTIME: "09:56:25.1534", TYPE: "DEPOSIT", JOBNAME: "BANK", UID: "PMIMAM0"},
		// 	SMF{SYSTEM: "CW09", REGION: "CICSR1", ZOSSERVERID: "FZID", STARTTIME: "09.56.06.1236", ENDTIME: "09:57:25.1534", TYPE: "CHKBAL", JOBNAME: "BANK", UID: "PMIMAM0"},
		// 	SMF{SYSTEM: "CW09", REGION: "CICSR2", ZOSSERVERID: "FZID", STARTTIME: "09.57.06.1236", ENDTIME: "09:58:25.1534", TYPE: "CHKBAL", JOBNAME: "BANK", UID: "PMIMAM0"},
		// 	SMF{SYSTEM: "CW11", REGION: "CICSR2", ZOSSERVERID: "FZID", STARTTIME: "09.58.06.1236", ENDTIME: "09:59:25.1534", TYPE: "WITHDRAW", JOBNAME: "BANK", UID: "SMIDXK0"},
		// 	SMF{SYSTEM: "CW11", REGION: "CICSR2", ZOSSERVERID: "FZID", STARTTIME: "09.59.06.1236", ENDTIME: "09:60:25.1534", TYPE: "WITHDRAW", JOBNAME: "BANK", UID: "SMIDXK0"},
		// 	SMF{SYSTEM: "CW11", REGION: "CICSR2", ZOSSERVERID: "FZID", STARTTIME: "10.00.06.1236", ENDTIME: "09:61:25.1534", TYPE: "CHKBAL", JOBNAME: "BANK", UID: "SMIDXK0"},
		// 	SMF{SYSTEM: "CW11", REGION: "CICSR2", ZOSSERVERID: "FZID", STARTTIME: "10.01.06.1236", ENDTIME: "09:62:25.1534", TYPE: "WITHDRAW", JOBNAME: "BANK", UID: "SMIDXK0"},
		// 	SMF{SYSTEM: "CW11", REGION: "CICSR2", ZOSSERVERID: "FZID", STARTTIME: "10.02.06.1236", ENDTIME: "09:63:25.1534", TYPE: "WITHDRAW", JOBNAME: "BANK", UID: "SMIDXK0"},
		// }

		smfs := []SMF{
			SMF{SRVR_JOBNAME: "PMIMAMFZ", SRVR_ID: "FZMM", SRVR_SYSTEM: "CW06", PRODNAME: "H06AC603", APPLID: "H06AC603", JOBNAME: "UCICS69M", TRAN: "ESDB", PROGRAMNAME: "ESDASM50", ORIGINATINGUSER: "CICSUSER", USERID: "CICSUSER", STARTTIME: "15.20.59.8978", ELAPSEDTIME: "00.08.03.3764"},
			SMF{SRVR_JOBNAME: "PMIMAMFZ", SRVR_ID: "FZMM", SRVR_SYSTEM: "CW06", PRODNAME: "H06AC603", APPLID: "H06AC603", JOBNAME: "UCICS69M", TRAN: "ESDB", PROGRAMNAME: "ESDASM50", ORIGINATINGUSER: "CICSUSER", USERID: "CICSUSER", STARTTIME: "15.20.59.8978", ELAPSEDTIME: "00.08.03.3764"},
			SMF{SRVR_JOBNAME: "PMIMAMFZ", SRVR_ID: "FZMM", SRVR_SYSTEM: "CW06", PRODNAME: "H06AC603", APPLID: "H06AC603", JOBNAME: "UCICS69M", TRAN: "ESDB", PROGRAMNAME: "ESDASM50", ORIGINATINGUSER: "CICSUSER", USERID: "CICSUSER", STARTTIME: "15.20.59.8978", ELAPSEDTIME: "00.08.03.3764"},
			SMF{SRVR_JOBNAME: "PMIMAMFZ", SRVR_ID: "FZMM", SRVR_SYSTEM: "CW06", PRODNAME: "H06AC603", APPLID: "H06AC603", JOBNAME: "UCICS69M", TRAN: "ESDB", PROGRAMNAME: "ESDASM50", ORIGINATINGUSER: "CICSUSER", USERID: "CICSUSER", STARTTIME: "15.20.59.8978", ELAPSEDTIME: "00.08.03.3764"},
			SMF{SRVR_JOBNAME: "PMIMAMFZ", SRVR_ID: "FZMM", SRVR_SYSTEM: "CW06", PRODNAME: "H06AC603", APPLID: "H06AC603", JOBNAME: "UCICS69M", TRAN: "ESDB", PROGRAMNAME: "ESDASM50", ORIGINATINGUSER: "CICSUSER", USERID: "CICSUSER", STARTTIME: "15.20.59.8978", ELAPSEDTIME: "00.08.03.3764"},
			SMF{SRVR_JOBNAME: "PMIMAMFZ", SRVR_ID: "FZMM", SRVR_SYSTEM: "CW06", PRODNAME: "H06AC603", APPLID: "H06AC603", JOBNAME: "UCICS69M", TRAN: "ESDB", PROGRAMNAME: "ESDASM50", ORIGINATINGUSER: "CICSUSER", USERID: "CICSUSER", STARTTIME: "15.20.59.8978", ELAPSEDTIME: "00.08.03.3764"},
			SMF{SRVR_JOBNAME: "PMIMAMFZ", SRVR_ID: "FZMM", SRVR_SYSTEM: "CW06", PRODNAME: "H06AC603", APPLID: "H06AC603", JOBNAME: "UCICS69M", TRAN: "ESDB", PROGRAMNAME: "ESDASM50", ORIGINATINGUSER: "CICSUSER", USERID: "CICSUSER", STARTTIME: "15.20.59.8978", ELAPSEDTIME: "00.08.03.3764"},
			SMF{SRVR_JOBNAME: "PMIMAMFZ", SRVR_ID: "FZMM", SRVR_SYSTEM: "CW06", PRODNAME: "H06AC603", APPLID: "H06AC603", JOBNAME: "UCICS69M", TRAN: "ESDB", PROGRAMNAME: "ESDASM50", ORIGINATINGUSER: "CICSUSER", USERID: "CICSUSER", STARTTIME: "15.20.59.8978", ELAPSEDTIME: "00.08.03.3764"},
			SMF{SRVR_JOBNAME: "PMIMAMFZ", SRVR_ID: "FZMM", SRVR_SYSTEM: "CW06", PRODNAME: "H06AC603", APPLID: "H06AC603", JOBNAME: "UCICS69M", TRAN: "ESDB", PROGRAMNAME: "ESDASM50", ORIGINATINGUSER: "CICSUSER", USERID: "CICSUSER", STARTTIME: "15.20.59.8978", ELAPSEDTIME: "00.08.03.3764"},
			SMF{SRVR_JOBNAME: "PMIMAMFZ", SRVR_ID: "FZMM", SRVR_SYSTEM: "CW06", PRODNAME: "H06AC603", APPLID: "H06AC603", JOBNAME: "UCICS69M", TRAN: "ESDB", PROGRAMNAME: "ESDASM50", ORIGINATINGUSER: "CICSUSER", USERID: "CICSUSER", STARTTIME: "15.20.59.8978", ELAPSEDTIME: "00.08.03.3764"},
		}

		// { RECORDS: [],
		// SRVR_JOBNAME: 'PMIMAMFZ',
		// SRVR_ID: 'FZMM',
		// SRVR_SYSTEM: 'CW06    ',
		// PRODNAME: 'H06AC603',
		// APPLID: 'H06AC603',
		// JOBNAME: 'UCICS69M',
		// TRAN: 'ESDB',
		// PROGRAMNAME: 'ESDASM50',
		// ORIGINATINGUSER: 'CICSUSER',
		// USERID: 'CICSUSER',
		// STARTTIME: '15.20.59.8978',
		// ELAPSEDTIME: '00.08.03.3764' },

		// args: [
		// 	smf.SRVR_JOBNAME,
		// 	smf.SRVR_ID,
		// 	smf.SRVR_SYSTEM,
		// 	smf.PRODNAME,
		// 	smf.APPLID,
		// 	smf.JOBNAME,
		// 	smf.TRAN,
		// 	smf.PROGRAMNAME,
		// 	smf.ORIGINATINGUSER,
		// 	smf.USERID,
		// 	smf.STARTTIME,
		// 	smf.ELAPSEDTIME
		// ],

	//Foreshadow Struct reference

		//SYSTEM string `json:"SYSTEM"`
		//REGION string `json:"REGION"`
		//ZOSSERVERID string `json:"ZOSSERVERID"`
		//STARTTIME string `json:"STARTTIME"`
		//ENDTIME string `json:"ENDTIME"`
		//TYPE string `json:"TYPE"`
		//JOBNAME string `json:"JOBNAME"`
		//UID string `json:"UID"`

	i := 0
	for i < len(smfs) {
		fmt.Println("i is ", i)
		carAsBytes, _ := json.Marshal(smfs[i])
		APIstub.PutState("SMF"+strconv.Itoa(i), carAsBytes)
		fmt.Println("Added", smfs[i])
		i = i + 1
	}

	return shim.Success(nil)
}

func (s *SmartContract) createSMF(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

fmt.Println(len(args));

	// if len(args) != 9 {
	// 	return shim.Error("Incorrect number of arguments. Expecting 9")
	// }

	if len(args) != 13 {
		return shim.Error("Incorrect number of arguments. Expecting 13")
	}

//http://localhost:3000/hyperledger/add/59d42f823c1df806035e5e09/cw09/PMIMAMFD/FZIS/10042017/10042017/WITHDRAW/ZZZZZZZZ/SMIDXK0

 //Foreshadow mods
 //DJK - Validate args match the initLedger formats otherwise certificates fail to create.
 //Struct reference

  //SYSTEM string `json:"SYSTEM"`
	//REGION string `json:"REGION"`
	//ZOSSERVERID string `json:"ZOSSERVERID"`
	//STARTTIME string `json:"STARTTIME"`
	//ENDTIME string `json:"ENDTIME"`
	//TYPE string `json:"TYPE"`
	//JOBNAME string `json:"JOBNAME"`
	//UID string `json:"UID"`

	//var smf = SMF{SYSTEM: args[1], REGION: args[2], ZOSSERVERID: args[3], STARTTIME: args[4], ENDTIME: args[5], TYPE: args[6], JOBNAME: args[7], UID: args[8]}

	var smf = SMF{SRVR_JOBNAME: args[1], SRVR_ID: args[2], SRVR_SYSTEM: args[3], PRODNAME: args[4], APPLID: args[5], JOBNAME: args[6], TRAN: args[7], PROGRAMNAME: args[8], ORIGINATINGUSER: args[9], USERID: args[10], STARTTIME: args[11], ELAPSEDTIME: args[12]}



	// args: [
	// 	smf.SRVR_JOBNAME,
	// 	smf.SRVR_ID,
	// 	smf.SRVR_SYSTEM,
	// 	smf.PRODNAME,
	// 	smf.APPLID,
	// 	smf.JOBNAME,
	// 	smf.TRAN,
	// 	smf.PROGRAMNAME,
	// 	smf.ORIGINATINGUSER,
	// 	smf.USERID,
	// 	smf.STARTTIME,
	// 	smf.ELAPSEDTIME
	// ],

	carAsBytes, _ := json.Marshal(smf)
	APIstub.PutState(args[0], carAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) queryAllSMF(APIstub shim.ChaincodeStubInterface) sc.Response {

	startKey := "SMF0"
	endKey := "SMF999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllSMF:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
